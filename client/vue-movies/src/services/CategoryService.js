import axios from 'axios';

export default class CategoryService {

    static baseurl = 'https://localhost:5001/categories/';

    static getAll() {
        return axios.get(this.baseurl)
            .then(response => {
                console.log(response);
                
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return [];
            });
    }
}