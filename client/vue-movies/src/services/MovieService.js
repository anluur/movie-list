import axios from 'axios';

export default class MovieService {

    static baseurl = 'https://localhost:5001/movies';

    static getAll(search, categoryIds) {

        let queryString = '';
        if (search != undefined || categoryIds != undefined) {
            queryString += '?';
            if (search != undefined) {
                queryString += 'search=' + search + '&';
            }
            if (categoryIds != undefined) {
                queryString += 'categoryIds=' + categoryIds.join(';');
            }
        }
        
        return axios.get(this.baseurl + queryString)
            .then(response => {
                console.log(response);
                
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return [];
            });
    }

    static get(id) {
        return axios.get(this.baseurl + '/details/' + id)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return null;
            });
    }
}