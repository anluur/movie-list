import Vue from 'vue';
import VueRouter from 'vue-router';
import MovieListView from '../views/MovieListView.vue';
import MovieDetails from '../views/MovieDetails.vue';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    redirect: { name: 'Movies' }
  },
  {
    path: '/movies',
    name: 'Movies',
    component: MovieListView
  },
  {
    path: '/movies/:id',
    name: 'MovieDetails',
    component: MovieDetails,
    props: true
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
