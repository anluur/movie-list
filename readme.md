## Dokumentatsioon

Proovitöö oli lahendatud kolme päeva jooksul ning selleks kuulus umbes 12-16 tundi. 

Backendi arenduseks oli kasutatud Jetbrains Rider, HTTP request'ide saatmiseks oli kasutatud Postman.

Frontend client arenduseks oli kasutatud VS Code, Vue JS, NPM, Bootstrap ja API suhtlemisega Axios teeki. Vue'd valisin, sest seda on lihtsam õppida ning olen ühte CRUD rakendust juba sellega teinud

### Lahendamise käik

#### Backend

REST API ASP .NET raamistiku abil arendamine ei olnud raske, sest ma olen seda õppinud. Samuti on minul arusaam sellistest kontseptidest nagu Repository pattern ja Dependecy Injection. Selle pärast püüdsin neist selles projektis rakendada. Praegu tegelen ma ülikooli raames projektiga, mis samuti rakendab ülal toodud disaini mustrit, ning proovitöös tegin midagi sarnast. Üldiselt polnud backendi poolest minu jaoks midagi uut.

#### Frontend

Kuna see on minu teine projekt, kus ma kasutan Vue'd, siis pidin õppima raamistikku ja selleks ma kasutasin Vue Mastery Intro to Vue.js kursi https://www.vuemastery.com/courses/intro-to-vue-js. Ka UI disainimisega pole mul palju kogemust, selle pärast taas kasutasin oma eelmise projekti css-i ja aktiivselt lugesin Bootstrapi dokumentatsiooni.

Kui sain teada, kuidas raamistikud töötavad, polnud minul tõsiseid probleemi ülesande lahendamisega. Huvitav oli teha autocomplete funktsionaalsust ja otsimis/filtreerimis funktsionaalsust.

## Project Description

### Backend

Backend is written using ASP .NET MVC core. Backend is in MovieList folder.
Backend app implements repository pattern and dependency injection, business logic in service classes, mappers and DTOs in separate classes.

Data - Movie list and Category list, are in Database project

Unit of work class is a container for repositories

Repositories take care of quering data from Database and returns entities defined in Domain. 

Services take of mapping domain entities to DTOs using Mappers as well as using unit of work to access Database thorugh repositories.

Unit of work, repositories, services, mappers are implement interfaces which are used for automatic dependency injection

Also api controllers were implemented that uses service for all business logic and are assigned to correspondng route.


#### Functionality:

* Get all movies by sending GET request to https://localhost:5001/movies, 

Data format:
```
[
    {
        "title": string,
        "rating": string,
        "year": string,
        "category": {
            "id": int,
            "name": string
        }
    }
]
```

* Search and filter movies by sending GET request to https://localhost:5001/movies?search=string&categoryIds=int;int, where both search and categoryIds parameters are optional; search is a string and categoryIds array of integers separated by semicolon.

Data format: same as when getting all

* Get single movie by sending GET request to https://localhost:5001/movies/details/id, (specify the id) 

Data format:
```
{
        "title": string,
        "rating": string,
        "year": string,
        "description": "string",
        "category": {
            "id": int,
            "name": string
        }
}
```

* Get all categories by sending GET request to https://localhost:5001/categories, 

Data format:
```
[
    {
        "id": int,
    "name": string
    } 
]
```

### Frontend

Frontend is written in vanilla JS with Vue.js framework, UI uses Bootstrap. Vue.js app sends requests to backend api. Frontend is located in client/vue-movies folder. 

Frontend provides UI and functionality for
* viewing list of movies
* searching movies by title (with autocomplete functionality)
* filtering movies by multiple categories
* viewing single movie details
* viewing error message if single movie was not found


