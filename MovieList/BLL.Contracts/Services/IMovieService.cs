﻿using System.Collections.Generic;
using DAL.Contracts;
using DTO;

namespace BLL.Contracts.Services
{
    public interface IMovieService: IBaseService<IAppUnitOfWork, MovieListItemDTO>
    {
        List<MovieListItemDTO> FindAll(string? search = null, int[]? categoryIds = null);
        MovieDetailedDTO FindById(int id);
    }
}