﻿using DAL.Contracts;
using Domain;

namespace BLL.Contracts.Services
{
    public interface ICategoryService: IBaseService<IAppUnitOfWork, Category>
    {
        
    }
}