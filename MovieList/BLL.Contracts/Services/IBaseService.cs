﻿using System.Collections.Generic;
using DAL.Contracts;

namespace BLL.Contracts.Services
{
    public interface IBaseService<TUnitOfWork, TDTO>
        where TUnitOfWork: class, IBaseUnitOfWork
        where TDTO: class, new()
    {
        List<TDTO> GetAll();
    }
}