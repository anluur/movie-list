﻿using Domain;
using DTO;

namespace BLL.Contracts.Mappers
{
    public interface IMovieMapper
    {
        MovieListItemDTO ToMovieListItemDTO(Movie movie, Category category);
        MovieDetailedDTO ToMovieDetailedDTO(Movie movie, Category category);
    }
}