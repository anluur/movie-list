﻿using System.Collections.Generic;
using Domain;

namespace Database
{
    public class CategoryList
    {
        public static List<Category> Categories => 
            new List<Category>()
            {
                new Category() { Id = 1, Name = "Action" },
                new Category() { Id = 2, Name = "Comedy" },
                new Category() { Id = 3, Name = "Crime" }, 
                new Category() { Id = 4, Name = "Drama" },
                new Category() { Id = 5, Name = "Fantasy" },
                new Category() { Id = 6, Name = "Horror" },
                new Category() { Id = 7, Name = "Romance" },
                new Category() { Id = 8, Name = "Science Fiction" },
                new Category() { Id = 9, Name = "Thriller" },
                new Category() { Id = 10, Name = "Western" },
                new Category() { Id = 11, Name = "Adventure" }
            };
    }
}