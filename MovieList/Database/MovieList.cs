﻿using System;
using System.Collections.Generic;
using Domain;

namespace Database
{
    public class MovieList
    {
        public static List<Movie> Movies => 
            new List<Movie>()
            {
                new Movie() { 
                    Id = 1,  
                    Title = "Logan", 
                    Rating = 8.1, 
                    Year = 2017, 
                    CategoryId = 1,
                    Description = "In a future where mutants are nearly extinct, an elderly and weary Logan leads a quiet life. " +
                                  "But when Laura, a mutant child pursued by scientists, comes to him for help, he must get her to safety. "
                },
                new Movie() { 
                    Id = 2,  
                    Title = "Inception", 
                    Rating = 8.8, 
                    Year = 2010, 
                    CategoryId = 9,
                    Description = "A thief who steals corporate secrets through the use of dream-sharing technology " +
                                  "is given the inverse task of planting an idea into the mind of a C.E.O. "
                },
                new Movie() { 
                    Id = 3,  
                    Title = "The Wolf of Wall Street", 
                    Rating = 8.2, 
                    Year = 2013, 
                    CategoryId = 4,
                    Description = "Based on the true story of Jordan Belfort, " +
                                  "from his rise to a wealthy stock-broker living the high life " +
                                  "to his fall involving crime, corruption and the federal government. "
                },
                new Movie() { 
                    Id = 4,  
                    Title = "Star Wars: Episode I - The Phantom Menace", 
                    Rating = 6.5, 
                    Year = 1999, 
                    CategoryId = 5,
                    Description = "Two Jedi escape a hostile blockade to find allies and come across " +
                                  "a young boy who may bring balance to the Force, " +
                                  "but the long dormant Sith resurface to claim their old glory. "
                },
                new Movie() { 
                    Id = 5,  
                    Title = "Annihilation", 
                    Rating = 6.9, 
                    Year = 2018, 
                    CategoryId = 4,
                    Description = "A biologist signs up for a dangerous, " +
                                  "secret expedition into a mysterious zone where the laws of nature don't apply. "
                },
                new Movie() { 
                    Id = 6,  
                    Title = "Indiana Jones and the Last Crusade", 
                    Rating = 8.2, 
                    Year = 1989, 
                    CategoryId = 11,
                    Description = "In 1938, after his father Professor Henry Jones, Sr. " +
                                  "goes missing while pursuing the Holy Grail, Professor Henry 'Indiana' Jones, Jr. " +
                                  "finds himself up against Adolf Hitler's Nazis again to stop them from obtaining its powers. "
                },
                new Movie() { 
                    Id = 7,  
                    Title = "Avengers: Infinity War", 
                    Rating = 8.5, 
                    Year = 2018, 
                    CategoryId = 1,
                    Description = "The Avengers and their allies must be willing to sacrifice " +
                                  "all in an attempt to defeat the powerful Thanos before his blitz of " +
                                  "devastation and ruin puts an end to the universe. "
                },
                new Movie() { 
                    Id = 8,  
                    Title = "Pulp Fiction", 
                    Rating = 8.9, 
                    Year = 1994, 
                    CategoryId = 3,
                    Description = "The lives of two mob hitmen, a boxer, a gangster and his wife, " +
                                  "and a pair of diner bandits intertwine in four tales of violence and redemption. "
                },
                new Movie() { 
                    Id = 9,  
                    Title = "The Godfather", 
                    Rating = 9.2, 
                    Year = 1972, 
                    CategoryId = 3,
                    Description = "The aging patriarch of an organized crime dynasty " +
                                  "transfers control of his clandestine empire to his reluctant son. "
                },
                new Movie() { 
                    Id = 10,  
                    Title = "The Shawshank Redemption", 
                    Rating = 9.3, 
                    Year = 1994, 
                    CategoryId = 4,
                    Description = "Two imprisoned men bond over a number of years, " +
                                  "finding solace and eventual redemption through acts of common decency. "
                }
            };
    }
}