﻿using System;

namespace DAL.Contracts
{
    public interface IBaseUnitOfWork
    {
        TRepository GetRepository<TRepository>(Func<TRepository> repoCreationMethod);
    }
}