﻿using System;
using DAL.Contracts.Repositories;

namespace DAL.Contracts
{
    public interface IAppUnitOfWork: IBaseUnitOfWork
    {
        
        IMovieRepository Movies { get;  }
        ICategoryRepository Categories { get;  }
    }
}