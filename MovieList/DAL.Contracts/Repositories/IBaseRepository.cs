﻿using System.Collections.Generic;

namespace DAL.Contracts.Repositories
{
    public interface IBaseRepository<T>
        where T: class, new()
    {
        List<T> GetAll();
        T FindById(int id);
    }
}