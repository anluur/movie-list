﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Domain;

namespace DAL.Contracts.Repositories
{
    public interface IMovieRepository : IBaseRepository<Movie>
    {
        List<Movie> FindAll(string? search, int[]? categoryIds);
    }
}