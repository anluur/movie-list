﻿using Domain;

namespace DAL.Contracts.Repositories
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        
    }
}