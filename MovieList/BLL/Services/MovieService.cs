﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Contracts.Mappers;
using BLL.Contracts.Services;
using DAL.Contracts;
using Domain;
using DTO;

namespace BLL
{
    public class MovieService: IMovieService
    {
        private readonly IAppUnitOfWork _uow;
        private readonly IMovieMapper _mapper;
        
        public MovieService(IAppUnitOfWork uow, IMovieMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }


        public List<MovieListItemDTO> GetAll()
        {
            return _uow.Movies.GetAll()
                .Select(m => _mapper.ToMovieListItemDTO(m, _uow.Categories.FindById(m.CategoryId)))
                .ToList();
        }

        public MovieDetailedDTO FindById(int id)
        {

            Movie movie;
            Category category;
            try
            {
                movie = _uow.Movies.FindById(id);
                category = _uow.Categories.FindById(movie.CategoryId);
            }
            catch (NullReferenceException e)
            {
                return null;
            }
            
            return _mapper.ToMovieDetailedDTO(movie, category);
        }

        public List<MovieListItemDTO> FindAll(string? search, int[]? categoryIds)
        {
            return _uow.Movies.FindAll(search, categoryIds)
                .Select(m => _mapper.ToMovieListItemDTO(m, _uow.Categories.FindById(m.CategoryId)))
                .ToList();
        }
    }
}