﻿using System.Collections.Generic;
using BLL.Contracts.Services;
using DAL.Contracts;
using Domain;

namespace BLL
{
    public class CategoryService: ICategoryService
    {
        private readonly IAppUnitOfWork _uow;
        
        public CategoryService(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        public List<Category> GetAll()
        {
            return _uow.Categories.GetAll();
        }
        
    }
}