﻿using System.Collections.Generic;
using BLL.Contracts.Mappers;
using Domain;
using DTO;

namespace BLL.Mappers
{
    public class MovieMapper: IMovieMapper
    {
        public MovieListItemDTO ToMovieListItemDTO(Movie movie, Category category)
        {
            return new MovieListItemDTO()
            {
                Id = movie.Id,
                Title = movie.Title,
                Year = movie.Year,
                Rating = movie.Rating,
                Category = category
            };
        }
        
        
        public MovieDetailedDTO ToMovieDetailedDTO(Movie movie, Category category)
        {
            return new MovieDetailedDTO()
            {
                Id = movie.Id,
                Title = movie.Title,
                Year = movie.Year,
                Description = movie.Description,
                Rating = movie.Rating,
                Category = category
            };
        }
    }
}