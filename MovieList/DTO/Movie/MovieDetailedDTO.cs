﻿using Domain;

namespace DTO
{
    public class MovieDetailedDTO
    {
        public int Id { get; set; }
        public string? Title { get; set; } = default!;
        public int Year { get; set; }
        public string? Description { get; set; } = default!;
        public double Rating { get; set; }
        public Category? Category { get; set; } = default!;
    }
}