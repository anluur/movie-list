﻿using System;
using Domain;

namespace DTO
{
    public class MovieListItemDTO
    {
        public int Id { get; set; }
        public string? Title { get; set; } = default!;
        public int Year { get; set; }
        public double Rating { get; set; }
        public Category? Category { get; set; } = default!;
    }
}