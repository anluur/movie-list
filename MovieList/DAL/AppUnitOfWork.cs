﻿using System;
using System.Collections.Generic;
using DAL.Contracts;
using DAL.Contracts.Repositories;
using DAL.Repositories;

namespace DAL
{
    public class AppUnitOfWork : IAppUnitOfWork
    {

        public IMovieRepository Movies =>
            GetRepository<IMovieRepository>(() => new MovieRepository());
        public ICategoryRepository Categories =>
            GetRepository<ICategoryRepository>(() => new CategoryRepository());
        
        
        private readonly Dictionary<Type, object> _repoCache = new Dictionary<Type, object>();

        public TRepository GetRepository<TRepository>(Func<TRepository> repoCreationMethod)
        {
            if (_repoCache.TryGetValue(typeof(TRepository), out var repo))
            {
                return (TRepository) repo;
            }

            repo = repoCreationMethod()!;
            _repoCache.Add(typeof(TRepository), repo!);
            return (TRepository) repo!;

        }
    }
}