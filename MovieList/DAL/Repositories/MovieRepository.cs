﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Contracts.Repositories;
using Database;
using Domain;

namespace DAL.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly List<Movie> _movies = MovieList.Movies;

        public List<Movie> GetAll()
        {
            return _movies;
        }

        public Movie FindById(int id)
        {
            return _movies.Find(m => m.Id == id);
        }

        public List<Movie> FindAll(string? search, int[]? categoryIds)
        {
            List<Movie> movies = _movies;
            if (categoryIds != null)
            {
                movies = movies.FindAll(m => categoryIds.Any(id => id == m.CategoryId));
            }
            if (search != null)
            {
                movies = movies.FindAll(m => m.Title.ToLower().Contains(search.ToLower()));
            }
            return movies;
        }
    }
}