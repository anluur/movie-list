﻿using System.Collections.Generic;
using System.Data;
using DAL.Contracts.Repositories;
using Database;
using Domain;

namespace DAL.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly List<Category> _categories = CategoryList.Categories;

        public List<Category> GetAll()
        {
            return _categories;
        }

        public Category FindById(int id)
        {
            return _categories.Find(c => c.Id == id);
        }
    }
}