﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Contracts.Services;
using DTO;
using Microsoft.AspNetCore.Mvc;
using ControllerBase = Microsoft.AspNetCore.Mvc.ControllerBase;

namespace WebApp.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _service;

        public MoviesController(IMovieService service)
        {
            _service = service;
        }


        // GET: /Movies
        public ActionResult<IEnumerable<MovieListItemDTO>> GetMovies(string? search, string? categoryIds)
        {
            List<MovieListItemDTO> movies;
            if (categoryIds != null)
            {
                int[] ids;
                try
                {
                    ids = categoryIds.Split(';').Select(int.Parse).ToArray();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine(e.StackTrace);
                    return BadRequest();
                }
                
                movies = _service.FindAll(search, ids);
            }
            else
            {
                movies = _service.FindAll(search);
            }

            return Ok(movies);
        }

        [ProducesResponseType(typeof(MovieDetailedDTO), 200)]
        [ProducesResponseType(404)]
        [HttpGet("details/{id}")]
        public ActionResult<MovieDetailedDTO> GetMovie(int id)
        {
            var movie = _service.FindById(id);

            if (movie == null)
            {
                return NotFound();
            }

            return Ok(movie);
        }
    }
}