﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Contracts.Services;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    
    [ApiController]
    [Route("[controller]")]

    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService _service;

        public CategoriesController(ICategoryService service)
        {
            _service = service;
        }


        // GET: /Categories
        [HttpGet]
        public ActionResult<IEnumerable<Category>> GetCategories()
        {
            var movies = _service.GetAll();

            return Ok(movies);
        }

    }
}